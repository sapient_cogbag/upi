-- This module actually manages the upi power system (which is very simple nya)
--
-- A UPI converter consists of 3 parts:
--  * interconnect core
--  * source converter
--  * target converter
--
-- The interconnect core, internally, manages most state.

-- Internal power network functions nya
upi.ipnw = {}
local S = upi.translator

-- When a source or target is destroyed, it notifies the core if it is connected to 
-- one to rescan after a small time (to allow it to recognise the destruction of 
-- the relevant source nya). 
local RESCAN_DELAY = 0.5

upi.ipnw.component = {}
upi.ipnw.component.source = "source"
upi.ipnw.component.target = "target"
upi.ipnw.component.core = "core"

upi.ipnw.meta = {}
-- Key holding the type of the node.
upi.ipnw.meta.type_key = "upi:type"

upi.ipnw.meta.core = {}

-- int value for throttling the throughput of a converter nya. If 0, then no throttling
-- happens. Negative values are treated as 0
upi.ipnw.meta.core.throttle = "upi:core_throttle"

-- String position values holding the associated target and source, if they exist
upi.ipnw.meta.core.source_pos = "upi:core_source"
upi.ipnw.meta.core.target_pos = "upi:core_target"

-- The last amount of power demanded/provided of the source - purely for deduplication 
-- of calls
upi.ipnw.meta.core.last_demanded = "upi:core_last_demanded"
upi.ipnw.meta.core.last_provided = "upi:core_last_provided"


upi.ipnw.meta.source = {}

-- Targets nya
upi.ipnw.meta.target = {}

-- Add fixed value for the fixed demand option in the target
upi.ipnw.meta.target.fixed_demand_number = "upi:target_fixed_demand"

-- bidi applies to both source and targets
upi.ipnw.meta.bidi = {}

-- Location of attached core
upi.ipnw.meta.bidi.core = "upi:converter_core"

-- Mode of the converter nya
upi.ipnw.meta.bidi.mode = "upi:converter_mode"


-- Holds functions for interfacing with an interconnect core 
upi.ipnw.core = {}

-- {{{ Core Functions

-- Initialise the given node as a core. Optional node and meta providable nya
--
-- pos, node, meta are the standard set of "optional node/meta" specifying the core
--
-- This also does an initial scan for sources/targets
--
-- Core also maintains the value of param2 - 0 = no source or target, 1 = either a 
-- source or target, 2 = both source and target. This is used to make it pretty ^.^
function upi.ipnw.core.initialise(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)

    meta:set_string(upi.ipnw.meta.type_key, upi.ipnw.component.core)
    meta:set_int(upi.ipnw.meta.core.throttle, 0)
    meta:set_int(upi.ipnw.meta.core.last_demanded, 0)
    meta:set_int(upi.ipnw.meta.core.last_provided, 0)

    upi.ipnw.core.scan_for_converters(pos, node, meta)
    upi.ipnw.core.refresh_infotext_string(pos, node, meta)
    meta:set_string(
        "formspec",
        "formspec_version[3]size[6,2]" ..
        "field[0.5,0.75;5,1;upi:core_throttle;" 
            .. minetest.formspec_escape(S("Throttle Power (UW)"))
            .. ";${" .. upi.ipnw.meta.core.throttle .. "}]"
        .. "tooltip[upi:core_throttle;" 
            .. minetest.formspec_escape(S("A throttle of zero turns off throttling"))
            .. "]"
    )
end

-- check if the location is actually a core.
function upi.ipnw.core.is_core(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)

    return node.name == "upi:interconnect_core" 
end

-- Check if the given core has a source that is valid. If it does, return the position
-- of that source
--
-- This now does full consistency check with all the metadata ^.^
function upi.ipnw.core.has_source(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    
    local maybe_source_position = 
        minetest.string_to_pos(meta:get_string(upi.ipnw.meta.core.source_pos))
    if not maybe_source_position then return end
    if upi.ipnw.source.is_source(maybe_source_position) 
        and upi.ipnw.bidi.is_our_core(maybe_source_position, nil, nil, pos, node, meta) then 
        return maybe_source_position 
    else
        -- clear the source as well for quicker future checks nya
        meta:set_string(upi.ipnw.meta.core.source_pos, "")
    end
end

-- Check if the given core has a target that is valid. If it does, return the position
-- of that target nya
--
-- This now does a full consistency check.
function upi.ipnw.core.has_target(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)

    local maybe_target_position = 
        minetest.string_to_pos(meta:get_string(upi.ipnw.meta.core.target_pos))
    if not maybe_target_position then return end
    if upi.ipnw.target.is_target(maybe_target_position) 
        and upi.ipnw.bidi.is_our_core(maybe_target_position, nil, nil, pos, node, meta) then 
        return maybe_target_position 
    else
        -- clear the target as well for quicker future checks nya
        meta:set_string(upi.ipnw.meta.core.target_pos, "")
    end
end

-- Refresh the infotext box on the core, and also other stuff 
-- like param2. Essentially this is "update the ui" function nya
--
-- Call this whenever the demand or provide changes nya
function upi.ipnw.core.refresh_infotext_string(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)

    local has_source = upi.ipnw.core.has_source(pos, node, meta)
    print("Has source? " .. tostring(has_source))
    local has_target = upi.ipnw.core.has_target(pos, node, meta)
    print("Has target? " .. tostring(has_target))
    
    local last_prov = meta:get_int(upi.ipnw.meta.core.last_provided)
    local last_dem = meta:get_int(upi.ipnw.meta.core.last_demanded)
   
    local new_param2 = 0


    -- No colour in infotext :(

    local resulting_infostring = ""
    
    if has_source then 
        resulting_infostring = resulting_infostring .. S("[Provided Power: %s]\n"):format( 
            upi.helpers.metric_prefix_format(last_prov) .. "UW"
        )
        new_param2 = new_param2 + 1
    else 
        resulting_infostring = resulting_infostring .. S("[No Source Converter]\n")
    end
    
    if has_target then
        resulting_infostring = resulting_infostring .. S("[Demanded Power: %s]"):format( 
            upi.helpers.metric_prefix_format(last_dem) .. "UW"
        )
        new_param2 = new_param2 + 1
    else
        resulting_infostring = resulting_infostring .. S("[No Target Converter]")
    end

    meta:set_string("infotext", resulting_infostring)

    if new_param2 ~= node.param2 then
        -- Nodes are passed around "by value" nya
        node.param2 = new_param2
        minetest.swap_node(pos, node)
    end
end

-- Demand the given number of UW from the source associated with the core.
--
-- Returns true if it was successfully demanded, false if the location was not actually a 
-- core nya
--
-- Final argument says to ignore the anti-same-demand-measures and re-demand regardless
--
-- This is mostly useful for refreshing the source :)
function upi.ipnw.core.demand(pos, node, meta, demanded_power, do_redemand)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    if not upi.ipnw.core.is_core(pos, node, meta) then return false end

    local throttle = math.max(meta:get_int(upi.ipnw.meta.core.throttle), 0)
    local actually_demanded = demanded_power

    local ldemand_key = upi.ipnw.meta.core.last_demanded

    if throttle ~= 0 then 
        actually_demanded = math.min(actually_demanded, throttle) 
    else

        -- Consider for a moment if two converters are attached in a cycle to a network, 
        -- and both are set to match target demand without throttle. This would cause an 
        -- infinite increase in the demand on each network until there began to be integer
        -- precision issues (noting that lua represents ints as floats in such a way that, 
        -- iirc, 2^52 is the limit of accuracy). To prevent this, we hard-bottleneck demand
        -- at 2^50 nya
        -- 4 bits is 1 hex digit, so 2^50 is 12 zeros then 0100 (4)
        -- 
        actually_demanded = math.min(actually_demanded, 0x4000000000000) 
    end
    
    -- Don't keep demanding if the demand is the same
    if meta:contains(ldemand_key) and 
        meta:get_int(ldemand_key) == actually_demanded and 
        not do_redemand 
    then
        -- Update infotext string nya
        upi.ipnw.core.refresh_infotext_string(pos, node, meta)
        return true
    end
    

    -- If we have a source, apply the demand to that source nya
    local source = upi.ipnw.core.has_source(pos, node, meta)

    if source then
        upi.ipnw.source.demand(source, nil, nil, actually_demanded)
    else
        upi.ipnw.core.provide(pos, node, meta, 0)
    end

    -- Then update last-demanded
    meta:set_int(ldemand_key, actually_demanded)
    -- And update the infotext
    upi.ipnw.core.refresh_infotext_string(pos, node, meta)
    return true
end

-- Re-demand from the source (returns the same as the simple demand function).
--
-- This is for whenever it is necessary to re-apply aspects of the source converter 
-- to it's associated network or other refresh events, or if the throttle changes.
function upi.ipnw.core.redemand_from_source(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    local ldemand_key = upi.ipnw.meta.core.last_demanded
    local last_demand = meta:get_int(ldemand_key)
    return upi.ipnw.core.demand(pos, node, meta, last_demand, true)
end


-- Provide the given number of UW to the core. If we have no target then tell the 
-- source that it no longer needs to provide anything.
--
-- Returns true if the power was successfully provided (the location was actually a core)
-- else return false
function upi.ipnw.core.provide(pos, node, meta, provided_power)
    local pos, node, meta = upi.pptriple(pos, node, meta) 
    if not upi.ipnw.core.is_core(pos, node, meta) then return false end

    local lprovide_key = upi.ipnw.meta.core.last_provided
    if meta:contains(lprovide_key) and meta:get_int(lprovide_key) == provided_power then
        return true
    end
    -- Now to try and get the target... If it isn't there then we set the demand to zero
    -- also nya
    --
    -- Preventing call cycles here is important - to do so, set last_provided to zero as 
    -- appropriate nya
    local maybe_target = upi.ipnw.meta.core.has_target(pos, node, meta)
    if maybe_target then
        upi.ipnw.target.provide(maybe_target, nil, nil, provided_power)
        -- Update infotext string nya
        upi.ipnw.core.refresh_infotext_string(pos, node, meta)
        return true
    else
        -- No target... need to do as described above nya
        meta:set_int(lprovide_key, 0)
        -- Update infotext string nya
        upi.ipnw.core.refresh_infotext_string(pos, node, meta)
        return upi.ipnw.core.demand(pos, node, meta, 0)
    end

end

-- Internal function for setting the target and calling any required handlers.
--
-- Note that this sets the target's core to ourselves
function upi.ipnw.core.set_target(pos, node, meta, target_pos) 
    local pos, node, meta = upi.pptriple(pos, node, meta)
    meta:set_string(upi.ipnw.meta.core.target_pos, minetest.pos_to_string(target_pos))
    upi.ipnw.bidi.set_core(target_pos, nil, nil, pos)
    upi.ipnw.core.refresh_infotext_string(pos, node, meta)
end


-- Internal function for setting the source and calling any required handlers ny
--
-- Note that this sets the source's core to ourselves
function upi.ipnw.core.set_source(pos, node, meta, source_pos) 
    local pos, node, meta = upi.pptriple(pos, node, meta)
    meta:set_string(upi.ipnw.meta.core.source_pos, minetest.pos_to_string(source_pos))
    upi.ipnw.bidi.set_core(source_pos, nil, nil, pos)
    upi.ipnw.core.refresh_infotext_string(pos, node, meta)
end

-- Schedule a poke if we haven't already done so
function upi.ipnw.core.schedule_poke(pos)
    local timer = minetest.get_node_timer(pos)
    if not timer:is_started() then
        timer:start(RESCAN_DELAY)
    end
end


-- Unset our source. This is called by sources when they are destroyed. It will 
-- schedule a poke around nya.
function upi.ipnw.core.unset_source(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    upi.ipnw.core.schedule_poke(pos)
    meta:set_string(upi.ipnw.meta.core.source_pos, "")
    upi.ipnw.core.refresh_infotext_string(pos, node, meta)
end

-- Unset our target. This is called by targets when they are destroyed. It will 
-- schedule a poke around nya.
function upi.ipnw.core.unset_target(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    upi.ipnw.core.schedule_poke(pos)
    meta:set_string(upi.ipnw.meta.core.target_pos, "")
    upi.ipnw.core.refresh_infotext_string(pos, node, meta)
end


-- Scan for source and target converters in adjacent spots and attempt to connect to
-- them if needed (and refresh the appropriate information). This is basically a "poke"
-- ;p
function upi.ipnw.core.scan_for_converters(pos, node, meta) 
    local pos, node, meta = upi.pptriple(pos, node, meta)
    -- This may be scheduled in the destruction of a core. Depending on how nodetimers
    -- work, this may still get run after destruction nya, when the node isn't a core, 
    -- which we don't want...
    if not upi.ipnw.core.is_core(pos, node, meta) then return end

    local adjacents = upi.adjacents(pos)
    
    if not upi.ipnw.core.has_target(pos, node, meta) then
        minetest.log("action", "[UPI] Interconnect Core [" .. 
            minetest.pos_to_string(pos) .. "] Scanning For Targets")
        for _, adj_pos in ipairs(adjacents) do
            -- Check if it is a target
            local tar_pos, tar_node, tar_meta = upi.pptriple(adj_pos)
            local tposstr = minetest.pos_to_string(tar_pos)
            
            local is_target = upi.ipnw.target.is_target(tar_pos, tar_node, tar_meta) 
            if not is_target then
                minetest.log("action", "[UPI] rejected " .. tposstr .. " - not a target converter")
            else
                if not upi.ipnw.bidi.has_core(tar_pos, tar_node, tar_meta) then
                    upi.ipnw.core.set_target(pos, node, meta, tar_pos)
                    minetest.log("action", "[UPI] accepted " .. tposstr)
                    break
                else    
                    minetest.log("action", "[UPI] rejected " .. tposstr .. " - has core")
                end
            end
        end
    end
    
    if not upi.ipnw.core.has_source(pos, node, meta) then
        minetest.log("action", "[UPI] Interconnect Core [" .. 
            minetest.pos_to_string(pos) .. "] Scanning For Sources")
        for _, adj_pos in ipairs(adjacents) do
            -- Check if it is a source
            local src_pos, src_node, src_meta = upi.pptriple(adj_pos)
            local sposstr = minetest.pos_to_string(src_pos)
            
            local is_source = upi.ipnw.source.is_source(src_pos, src_node, src_meta) 
            if not is_source then
                minetest.log("action", "[UPI] rejected " .. sposstr .. " - not a source converter")
            else
                if not upi.ipnw.bidi.has_core(src_pos, src_node, src_meta) then
                    upi.ipnw.core.set_source(pos, node, meta, src_pos)
                    minetest.log("action", "[UPI] accepted " .. sposstr)
                    break
                else    
                    minetest.log("action", "[UPI] rejected " .. sposstr .. " - has core")
                end
            end
        end
    end

end


-- This schedules the cores around any existing source or target to poke. 
--
-- We can't use NodeTimerRef on the source/target converters because it is not controlled
-- by us, so instead we need to go to any surrounding nodes and prompt their poking nya
function upi.ipnw.core.on_destroy(pos, node, meta) 
    local pos, node, meta = upi.pptriple(pos, node, meta)
    local maybe_source = upi.ipnw.core.has_source(pos, node, meta)
    
    if maybe_source then 
        -- Remove ourselves from its core entry
        local spos, snode, smeta = upi.pptriple(maybe_source)
        upi.ipnw.bidi.unset_core(spos, snode, smeta) 
    end

    local maybe_target = upi.ipnw.core.has_target(pos, node, meta)

    if maybe_target then
        local tpos, tnode, tmeta = upi.pptriple(maybe_target)
        upi.ipnw.bidi.unset_core(tpos, tnode, tmeta) 
    end
end

-- }}}


-- {{{ Source Only Functions
upi.ipnw.source = {}



-- Initialise the given node as a source.
--
-- This pokes any nearby cores as well
function upi.ipnw.source.initialise(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)

    meta:set_string(upi.ipnw.meta.type_key, upi.ipnw.component.source)
    meta:set_string(upi.ipnw.meta.bidi.core, "")
    meta:set_string(
        upi.ipnw.meta.bidi.mode, 
        upi.api.get_network_class(node.name):get_default_source_mode()
    )

    upi.ipnw.bidi.scan_and_poke_cores(pos, node, meta) 
end

-- Check if the node actually is a source... nya
function upi.ipnw.source.is_source(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    return upi.registry.converters[node.name] ~= nil 
        and upi.registry.converters[node.name].converter_type == upi.api.contype.src
end

-- Demand from the source, this also helps with providing power too! nya! 
--
-- If the provided location isn't a source, then error out...
function upi.ipnw.source.demand(pos, node, meta, demanded_power)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    assert(
        upi.ipnw.source.is_source(pos, node, meta), 
        "Attempted to demand power from something that isn't a source :( nya"
    )
    local maybe_core = upi.ipnw.bidi.has_core(pos, node, meta)
    local mode = upi.ipnw.bidi.get_mode(pos, node, meta)
    local actual_demand = demanded_power

    -- No core, no real demand! nya
    if not maybe_core then actual_demand = 0 end

    local network_class = upi.api.get_network_class(node.name)
    assert(network_class, "source doesn't have network class???")

    -- not nil since we used get_mode which ensures validity :) nya
    local mode_function = network_class:is_source_mode(mode) 

    -- Network
    local net = network_class:network_connected(pos, node, meta)
    

    if not net then
        -- Not actually connected - this means we must provide 0.
        if maybe_core then upi.ipnw.core.provide(maybe_core, nil, nil, 0) end
        return 
    end

    local network_base = network_class:get_network_base()
    if network_base == upi.api.nw.nettype.energy then
        assert(false, "Energy-based networks not currently implemented! :( nya")
    elseif network_base == upi.api.nw.nettype.power then
        -- The actual demand is done in mode_function
        local new_provide = mode_function(pos, node, meta, net, actual_demand)
        -- If there is no core then we provide 0 anyway so doesn't need to do anything nya
        if maybe_core then 
            upi.ipnw.core.provide(maybe_core, nil, nil, new_provide)
        end
    else
        assert(false, "Invalid nettype")
    end
end

-- Refresh this source's provided power from the network if it has any nya
--
-- In particular this essentially executes a redemand nya. It should be used on:
-- * Source Network connects/disconnects
-- * Mode changes
-- * Power network changes or events nya (both in terms of calculating amounts we demand
--  and also the amount provided nya)
function upi.ipnw.source.refresh_all_information(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    local maybe_core_pos = upi.ipnw.bidi.has_core(pos, node, meta)

    -- We have a core: redemands from it
    if maybe_core_pos then
        upi.ipnw.core.redemand_from_source(maybe_core_pos, nil, nil)
    else -- No core - redemand with 0 nya
        upi.ipnw.source.demand(pos, node, meta, 0)
    end
end


-- Function on destroying the source.
--
-- This schedules pokes as well as ensuring that no power is provided nya
function upi.ipnw.source.on_destroy(pos, node, meta) 
    local pos, node, meta = upi.pptriple(pos, node, meta)
    local maybe_core_pos = upi.ipnw.bidi.has_core(pos, node, meta)
    if maybe_core_pos then
        -- Remove ourselves from the core's source nya
        local maybe_core_pos, cnode, cmeta = upi.pptriple(maybe_core_pos)
        if upi.ipnw.core.is_core(maybe_core_pos, cnode, cmeta) then
            upi.ipnw.core.provide(maybe_core_pos, cnode, cmeta, 0)
            upi.ipnw.core.unset_source(maybe_core_pos, cnode, cmeta)
        end
    end
end



-- Function for setting the mode of a source to the new mode.
--
-- If the converter has an element upi_after_set_mode(pos, node, meta) in the node definition,
-- this will be called. It can be used to regenerate the formspec nya (this is only
-- required because you can't use variable references in the index field of dropdowns 
-- :( )
--
-- Invalid modes will be read as default (they are blindly set because get_mode 
-- autochecks validity every time). 
function upi.ipnw.source.set_mode(pos, node, meta, new_mode)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    local did_set = false
    if type(new_mode) == type("str") then 
        meta:set_string(upi.ipnw.meta.bidi.mode, new_mode)
        did_set = true
    end

    upi.ipnw.source.refresh_all_information(pos, node, meta)

    local node_definition = minetest.registered_nodes[node.name]
    if node_definition and type(node_definition.upi_after_set_mode) == "function" and did_set then
        node_definition.upi_after_set_mode(pos, node, meta)
    end
end

-- }}}

upi.ipnw.target = {}

-- {{{ Target Only Functions

-- Initialise the given node as a target
--
-- Also scans/pokes cores nya
function upi.ipnw.target.initialise(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)

    meta:set_string(upi.ipnw.meta.type_key, upi.ipnw.component.target) 
    meta:set_string(upi.ipnw.meta.bidi.core, "")
    meta:set_string(
        upi.ipnw.meta.bidi.mode, 
        upi.api.get_network_class(node.name):get_default_source_mode()
    )
    
    -- Default is 10,000 UW
    meta:set_int(upi.ipnw.meta.target.fixed_demand_number, 10000)
    
    upi.ipnw.bidi.scan_and_poke_cores(pos, node, meta) 
end

-- Check if the node actually is a target... nya
function upi.ipnw.target.is_target(pos, node, meta)
    local node = node or minetest.get_node_or_nil(pos)
    if node == nil then return end

    return upi.registry.converters[node.name] ~= nil 
        and upi.registry.converters[node.name].converter_type == upi.api.contype.tgt
end


-- Set the amount provided to a target converter of this network nya
-- Note that this does not update the target at all other than the supply ;p
function upi.ipnw.target.provide(pos, node, meta, provided_upiwatts)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    assert(upi.ipnw.target.is_target(pos, node, meta), "Attempted to provide upiWatts to non-target nya")
    local network_class = upi.api.get_network_class(node.name)
    -- If it's nil then this isn't actually a target or something... nya
    assert(network_class ~= nil, "Target didn't have network type/class????") 
    
    -- Network
    local net = network_class:network_connected(pos, mode, meta)
    -- Not actually connected - this means all provided power is wasted.
    if not net then return end

    local network_base = network_class:get_network_base()
    if network_base == upi.api.nw.nettype.energy then
        assert(false, "Energy-Based networks not currently supported! nya!")
    elseif network_base == upi.api.nw.nettype.power then
        network_class:supply_upi(pos, node, meta, provided_upiwatts)
    else
        assert(false, "Invalid nettype >.< nya")
    end
end


-- Set the fixed demand value ^.^
--
-- This also checks: 
--  * if it is an int
--  * if it is >= 0 nya
function upi.ipnw.target.set_fixed_demand(pos, node, meta, new_fixed_demand)
    if type(new_fixed_demand) == type(10) and new_fixed_demand > 0 then
        local actual_nfd = math.floor(new_fixed_demand)
        local pos, node, meta = upi.pptriple(pos, node, meta)
        meta:set_int(upi.ipnw.meta.target.fixed_demand_number, actual_nfd)
        -- Then rescan/recalculate stuff ;p nya
        upi.ipnw.target.refresh_all_information(pos, node, meta)
    end
end

-- Get the fixed demand value ;p
function upi.ipnw.target.get_fixed_demand(pos, node, meta) 
    local pos, node, meta = upi.pptriple(pos, node, meta)
    return meta:get_int(upi.ipnw.meta.target.fixed_demand_number)
end

-- Refresh the information from the current network if it exists.. nya
--
-- This should be called on:
-- * network connects/disconnects
-- * mode changes
-- * fixed value changes (this is handled automatically) 
-- * network changes or general updates
function upi.ipnw.target.refresh_all_information(pos, node, meta) 
    local pos, node, meta = upi.pptriple(pos, node, meta)
    assert(upi.ipnw.target.is_target(pos, node, meta), "Attempt to refresh target info on non target >:( nya")
    local maybe_core = upi.ipnw.bidi.has_core(pos, node, meta)
    local network_class = upi.api.get_network_class(node.name)
    if not network_class then assert("Target has no network class??? nya") end

    -- Network
    local net = network_class:network_connected(pos, node, meta)
    if not net then
        -- Not actually connected - this means all provided power is wasted.
        --
        -- Set demand to nil.
        if maybe_core then upi.ipnw.core.demand(maybe_core, nil, nil, 0) end
        return
    end
    
    -- Always valid because get_mode.. nya
    local mode = network_class:is_target_mode(upi.ipnw.bidi.get_mode(pos, node, meta))
    local network_base = network_class:get_network_base()
    
    if network_base == upi.api.nw.nettype.energy then
        assert(false, "Energy networks unsupported ;-; nya")
    elseif network_base == upi.api.nw.nettype.power then
        local fixed_demand_num = upi.ipnw.target.get_fixed_demand(pos, node, meta)
        local new_demand = mode(pos, node, meta, net, fixed_demand_num)
        if maybe_core then 
            upi.ipnw.core.demand(maybe_core, nil, nil, new_demand)
        end
    else
        assert(false, "Unknown nettype")
    end 
end


-- Function on destroying the target.
--
-- This schedules pokes as well as ensuring that no demand is remaining
function upi.ipnw.target.on_destroy(pos, node, meta) 
    local pos, node, meta = upi.pptriple(pos, node, meta)
    print("destroying target")
    local maybe_core_pos = upi.ipnw.bidi.has_core(pos, node, meta)
    if maybe_core_pos then
        local cpos, cnode, cmeta = upi.pptriple(maybe_core_pos)
        if upi.ipnw.core.is_core(cpos, cnode, cmeta) then
            print("core was core nya")
            upi.ipnw.core.unset_target(cpos, cnode, cmeta)
        else 
            print("core was not core?")
        end
    else
        print("no core?")
    end
end

-- Function for setting the mode of a target to the new mode.
--
-- If the converter has an element upi_after_set_mode(pos, node, meta) in the node definition,
-- this will be called. It can be used to regenerate the formspec nya (this is only
-- required because you can't use variable references in the index field of dropdowns 
-- :(
--
-- Invalid modes will be read as default (they are blindly set because get_mode 
-- autochecks validity every time)
--
-- new_fixed_data is checked more thoroughly and autoconverted to int where possible 
-- nya. If it can't be, then it's ignored
function upi.ipnw.target.set_mode(pos, node, meta, new_mode, new_fixed_data)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    local did_set = false
    if type(new_mode) == type("str") then 
        meta:set_string(upi.ipnw.meta.bidi.mode, new_mode)
        did_set = true
    end


    if new_fixed_data and type(new_fixed_data) ~= type(10) then 
        new_fixed_data = tonumber(new_fixed_data)
    end

    if new_fixed_data and new_fixed_data >= 1 then
        meta:set_int(upi.ipnw.meta.target.fixed_demand_number, new_fixed_data)
        did_set = true
    end

    upi.ipnw.target.refresh_all_information(pos, node, meta)

    local node_definition = minetest.registered_nodes[node.name]
    if node_definition and type(node_definition.upi_after_set_mode) == "function" and did_set then
        node_definition.upi_after_set_mode(pos, node, meta)
    end
end

-- }}}



-- {{{ Functions applying to sources and targets both but not cores nya
upi.ipnw.bidi = {}


-- Read the current mode from metadata. If the current mode is invalid, 
-- then return the default nya. If the node is not a 
-- source or target then get none nya
function upi.ipnw.bidi.get_mode(pos, node, meta)  
    local pos, node, meta = upi.pptriple(pos, node, meta)

    local mode = meta:get_string(upi.ipnw.meta.bidi.mode)
    local network_class = upi.api.get_network_class(node.name)
    if network_class == nil then return end

    if upi.ipnw.target.is_target(pos, node, meta) then
        if network_class:is_target_mode(mode) then 
            return mode 
        else 
            return network_class:get_default_target_mode()
        end

    elseif upi.ipnw.source.is_source(pos, node, meta) then
        if network_class:is_source_mode(mode) then
            return mode
        else
            return network_class:get_default_source_mode()
        end
    end
end

-- Test if the converter has a core or not. If it does, then return the location of it
function upi.ipnw.bidi.has_core(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    local maybe_core_loc = minetest.string_to_pos(meta:get_string(upi.ipnw.meta.bidi.core))
    if not maybe_core_loc then return nil end
    if upi.ipnw.core.is_core(maybe_core_loc) then return maybe_core_loc else return nil end 
end

-- Function to be called on network changes and mode changes and stuff ^.^ nya
function upi.ipnw.bidi.refresh_all_information(pos, node, meta) 
    local pos, node, meta = upi.pptriple(pos, node, meta)
    if upi.ipnw.source.is_source(pos, node, meta) then
        upi.ipnw.source.refresh_all_information(pos, node, meta)
    elseif upi.ipnw.target.is_target(pos, node, meta) then
        upi.ipnw.target.refresh_all_information(pos, node, meta)
    end
end

-- Set the core ^.^. All set/unset functions are managed by the core itself
function upi.ipnw.bidi.set_core(pos, node, meta, core_pos)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    -- Set as core for source/target node nya
    meta:set_string(upi.ipnw.meta.bidi.core, minetest.pos_to_string(core_pos))
    upi.ipnw.bidi.refresh_all_information(pos, node, meta)
    -- Call the on_upi_core_state function.
    local csh = minetest.registered_nodes[node.name].on_upi_core_state
    if type(csh) == "function" then
        csh(pos, node, meta, true)
    end
end

-- Unset our core.
function upi.ipnw.bidi.unset_core(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    meta:set_string(upi.ipnw.meta.bidi.core, "")
    upi.ipnw.bidi.refresh_all_information(pos, node, meta)
    -- Call the on_upi_core_state function.
    local csh = minetest.registered_nodes[node.name].on_upi_core_state
    if type(csh) == "function" then
        csh(pos, node, meta, false)
    end
    -- Schedule any nearby cores to poke us nya
    upi.ipnw.bidi.schedule_surrounding_core_poke(pos, node, meta)
end

-- Scan around for cores and when they are found, poke them to try connections (if 
-- we don't have a core)
function upi.ipnw.bidi.scan_and_poke_cores(pos, node, meta) 
    local pos, node, meta = upi.pptriple(pos, node, meta) 
    if not upi.ipnw.bidi.has_core(pos, node, meta) then
        local adjacents = upi.adjacents(pos, node, meta)
        for _, adj_pos in ipairs(adjacents) do
            local cpos, cnode, cmeta = upi.pptriple(adj_pos)
            if upi.ipnw.core.is_core(cpos, cnode, cmeta) then
                -- Poke! :3
                upi.ipnw.core.scan_for_converters(cpos, cnode, cmeta)
            end
        end
    end
end

-- Schedule the parts around us to try poking in a little bit, regardless of 
-- whether we currently have a core (we expect to not have one soon nya)
function upi.ipnw.bidi.schedule_surrounding_core_poke(pos, _node, _meta) 
    local adjacents = upi.adjacents(pos)
    for _, adj_pos in ipairs(adjacents) do
        if upi.ipnw.core.is_core(adj_pos, nil, nil) then
            upi.ipnw.core.schedule_poke(adj_pos)
        end
    end
end

-- Test if we have a core AND that the core given is the core we are connected to nya
--
-- This essentially acts as a FULL consistency check on bidi <-> core association, which
-- is good for cases where there are weird asynchrony issues (like has_source/target i 
-- think does?). Basically if ANY of the metadata components mismatch in either the 
-- bidi or the core it will return false nya.
function upi.ipnw.bidi.is_our_core(pos, node, meta, core_pos, core_node, core_meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    if not upi.ipnw.bidi.has_core(pos, node, meta) then return false end

    -- Checking that our own core matches the argument
    local our_core_position = 
        minetest.string_to_pos(meta:get_string(upi.ipnw.meta.bidi.core))
    if not our_core_position or not vector.equals(our_core_position, core_pos) then return false end

    -- Checking that the core holds ourselves nya
    local cpos, cnode, cmeta = upi.pptriple(core_pos, core_node, core_meta)
    local core_reported_source_position = 
        minetest.string_to_pos(cmeta:get_string(upi.ipnw.meta.core.source_pos))
    local core_reported_target_position = 
        minetest.string_to_pos(cmeta:get_string(upi.ipnw.meta.core.target_pos))

    -- Has no targets or sources
    if not core_reported_source_position and not core_reported_target_position then return false end

    -- Check that we are one of source or target nya
    return (core_reported_source_position and vector.equals(core_reported_source_position, pos))
        or (core_reported_target_position and vector.equals(core_reported_target_position, pos))
end

-- }}}





-- upi
-- Copyright (C) 2021  sapient_cogbag <sapient_cogbag@protonmail.com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
