-- Mod data table nya
upi = {}
upi.modpath = minetest.get_modpath("upi")
upi.translator = minetest.get_translator("upi")
local S = upi.translator

-- Preparse a (pos, [maybe_nil] node, [maybe_nil] meta) tuple.
function upi.pptriple(pos, node, meta) 
    local node = node or minetest.get_node_or_nil(pos)
    local meta = meta or minetest.get_meta(pos)

    return pos, node, meta
end

-- Get all the positions adjacent to the given in the order +Y -Y +X -X +Z -Z
function upi.adjacents(pos, node, meta)
    return {
        vector.offset(pos, 0, 1, 0),
        vector.offset(pos, 0, -1, 0),
        vector.offset(pos, 1, 0, 0),
        vector.offset(pos, -1, 0, 0),
        vector.offset(pos, 0, 0, 1),
        vector.offset(pos, 0, 0, -1)
    }
end

-- Optional dependencies to scan through and mark out nya
local optdeps = {
    "default",
    "basic_materials",
    "mesecons",
    "microexpansion",
    "technic"
}

-- Map of modname -> modpath: can of course use as boolean check (that's the primary purpose)
upi.has_mod = {}

for _, modname in ipairs(optdeps) do
    upi.has_mod[modname] = minetest.get_modpath(modname)
end

-- Helpers
dofile(upi.modpath.."/helpers.lua")

-- API for the local UPI power system nya
dofile(upi.modpath.."/upi_power.lua")

dofile(upi.modpath.."/register_interconnect_core.lua")

-- API for managing power conversion
upi.api = {}
dofile(upi.modpath.."/upi_api.lua")

local mod_modules = {
    technic = "technic.lua"
}

-- Table containing if the converters should be enabled for a given mod nya
upi.api.enable_converters = {}

-- Load network registration and converters for all mods we have implementations for ;p
for dep, v in pairs(upi.has_mod) do
    if v and mod_modules[dep] then
        upi.api.enable_converters[dep] = minetest.settings:get_bool("upi_converters_" .. dep)
    end
end

for mod_name, convenabled in pairs(upi.api.enable_converters) do
    if convenabled then
        minetest.log("action", "Loading mod module '" .. mod_modules[mod_name] .. "'.")
    else
        minetest.log("action", "Loading mod module '" .. mod_modules[mod_name] .. "' without converters.")
    end
    dofile(upi.modpath .. "/implementations/" .. mod_modules[mod_name])
end


-- upi
-- Copyright (C) 2021  sapient_cogbag <sapient_cogbag@protonmail.com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
