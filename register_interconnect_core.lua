-- Manages the core interconnect block used in the rest of the mod ^.^

local S = upi.translator

minetest.register_node("upi:interconnect_core", {
    description = S("Interconnect Core"),
    tiles = {"upi_interconnect_core_dynamic_underlay_texture.png"},
    palette = "upi_interconnect_core_dynamic_underlay_palette.png",
    overlay_tiles = {
        {name = "upi_interconnect_core_overlay.png", color="white"}
    },
    paramtype2 = "color",
    drop = "upi:interconnect_core",
    use_texture_alpha = "clip",
    groups = {cracky=2, oddly_breakable_by_hand=2},
    on_receive_fields = function (pos, _formname, fields, _sender)

        local raw_string_throttle = fields["upi:core_throttle"]
        if not raw_string_throttle then return end

        local as_int = tonumber(raw_string_throttle)
        if not as_int then return end
        as_int = math.floor(as_int)
        if as_int < 0 then return end


        local meta = minetest.get_meta(pos)
        if meta then meta:set_int(upi.ipnw.meta.core.throttle, as_int) end
        -- Then finally redemand ourselves
        upi.ipnw.core.redemand_from_source(pos, nil, meta)
    end,
    on_construct = function(pos)
        local pos, node, meta = upi.pptriple(pos)
        upi.ipnw.core.initialise(pos, node, meta)
    end,
    on_destruct = function(pos)
        local pos, node, meta = upi.pptriple(pos)
        upi.ipnw.core.on_destroy(pos, node, meta)
    end,
    place_param2 = 0,
    -- Scan for converters nya ;p
    on_timer = function(pos, _elapsed)
        local pos, node, meta = upi.pptriple(pos)
        upi.ipnw.core.scan_for_converters(pos)
        return false
    end
})

-- Adding default-derived recipe. Note that this is *meant* to be cheap because 
-- power conversion is meant to be fairly easy to do to avoid constantly duplicating 
-- generation methods nya
--
-- For some reason wrought iron is called steel_ingot in the `default` mod >.<
if upi.has_mod["default"]  and not upi.has_mod["basic_materials"] then 
    minetest.register_craft({
        type="shaped",
        output="upi:interconnect_core 3",
        recipe = {
            {"default:copper_ingot", "default:flint"      , "default:copper_ingot"},
            {"default:copper_ingot", "default:steel_ingot", "default:copper_ingot"},
            {"default:copper_ingot", "default:flint"      , "default:copper_ingot"}
        }
    })
-- Better recipe using spools of wire ^.^
elseif upi.has_mod["basic_materials"] then
    minetest.register_craft({
        output = "upi:interconnect_core 3",
        recipe = {
            { "default:steel_ingot",        "default:flint",           "default:steel_ingot" },
            { "basic_materials:copper_wire","basic_materials:silicon", "basic_materials:copper_wire" },
            { "default:steel_ingot",        "default:flint",           "default:steel_ingot" }
        },
        replacements = {
            { "basic_materials:copper_wire", "basic_materials:empty_spool" },
            { "basic_materials:copper_wire", "basic_materials:empty_spool" },
        }
    })
end

-- upi
-- Copyright (C) 2021  sapient_cogbag <sapient_cogbag@protonmail.com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
