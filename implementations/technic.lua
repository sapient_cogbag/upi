-- Implementation of power networks and source/target converters for technic nya
local tiers = {}
local S = upi.translator
tiers.HV = "HV"
tiers.MV = "MV"
tiers.LV = "LV"

-- Ratios - we try to respect the lossiness of going up a tier with a native converter
-- (tho we can't really do this the other direction nya)
local tier_ratios = {}
tier_ratios[tiers.HV] = 96000
tier_ratios[tiers.MV] = 86400
tier_ratios[tiers.LV] = 77760

for _, tier in pairs(tiers) do
    -- Argument to upi.api.nw.register_power_network
    local register_table = {}
    
    function register_table.modify_register_node_definition(_name, def, _conn_type)
        if not def.groups then def.groups = {} end
        def.groups["technic_" .. string.lower(tier)] = 1
        def.groups["technic_machine"] = 1
        def.connect_sides = {"top", "bottom", "left", "right","front", "back"}
        return def
    end

    function register_table.register_connectable(node_name, connection_type)
        if connection_type == upi.api.contype.receiver then
            technic.register_machine(tier, node_name, technic.receiver)
        elseif connection_type == upi.api.contype.provider then
            technic.register_machine(tier, node_name, technic.producer)
        else return
        end
    end

    function register_table.set_source_demand(pos, node, meta, net, new_demand)
        meta:set_int(tier .. "_EU_demand", new_demand)
    end

    function register_table.get_received_power(pos, node, meta, net)
        return meta:get_int(tier .. "_EU_input") or 0
    end

    function register_table.set_target_supply(pos, node, meta, net, new_supply)
        meta:set_int(tier .. "_EU_supply", new_supply)
    end

    upi.api.nw.register_power_network("technic:" .. tier, tier_ratios[tier], register_table) 
end

local function define_converters()
    for _, tier in pairs(tiers) do
        local tier_nw = upi.api.nw.get_registered_network("technic:" .. tier)
        
        local cable_name = "technic:" .. string.lower(tier) .. "_cable"
        local src_node_name = "upi:technic_" .. string.lower(tier) .. "_source_converter" 
        local tgt_node_name = "upi:technic_" .. string.lower(tier) .. "_target_converter"

        tier_nw:register_converter(src_node_name, {
            description = S(tier .. " Technic Source Converter"),
            

            tiles = {{name="upi_interconnect_bidi_underlay.png"}},
            -- Grab the wire overlay nya
            overlay_tiles = {{
                name="upi_interconnect_source_base.png^technic_cable_connection_overlay.png", 
                color="white"
            }},
            palette="upi_interconnect_bidi_palette.png",
            -- We are doing similar to `core` here - we use the on_upi_core_state to maintain 
            -- param2
            paramtype2 = "color",
            -- Disable colour transfer
            drop = src_node_name,
            -- Need this for overlays to work properly ^.^
            use_texture_alpha = "clip",
            place_param2 = 0,
            groups = {cracky=2, oddly_breakable_by_hand=2},
            

            on_construct = function(pos)
                local pos, node, meta = upi.pptriple(pos)
                tier_nw:init_converter(pos, node, meta)
                meta:set_string("formspec", tier_nw:basic_converter_formspec(pos, node, meta))
            end,
            on_destruct = function(pos)
                local pos, node, meta = upi.pptriple(pos)
                tier_nw:destroy_converter(pos, node, meta)
            end,
            on_receive_fields = function(pos, _formname, fields, _sender)
                local pos, node, meta = upi.pptriple(pos)
                tier_nw:source_converter_receive_form(pos, node, meta, fields)
            end,
            technic_run = function (pos)
                local pos, node, meta = upi.pptriple(pos)
                tier_nw:refresh_network_information_converter(pos, node, meta)
                -- Reset/refresh the formspec
                meta:set_string("formspec", tier_nw:basic_converter_formspec(pos, node, meta))
            end,
            on_upi_core_state = function(pos, node, meta, has_core_now) 
                if has_core_now and node.param2 == 0 then
                    node.param2 = 1
                    minetest.swap_node(pos, node)
                elseif not has_core_now and node.param2 == 1 then
                    node.param2 = 0
                    minetest.swap_node(pos, node)
                end
            end
        }, upi.api.contype.src)
        
        tier_nw:register_converter(tgt_node_name, {
            description = S(tier .. " Technic Target Converter"),
            

            tiles = {{name="upi_interconnect_bidi_underlay.png"}},
            -- Grab the wire overlay nya
            overlay_tiles = {{
                name="upi_interconnect_target_base.png^technic_cable_connection_overlay.png", 
                color="white"
            }},
            palette="upi_interconnect_bidi_palette.png",
            -- We are doing similar to `core` here - we use the on_upi_core_state to maintain 
            -- param2
            paramtype2 = "color",
            -- Disable colour transfer
            drop = tgt_node_name,
            -- Need this for overlays to work properly ^.^
            use_texture_alpha = "clip",
            place_param2 = 0,
            groups = {cracky=2, oddly_breakable_by_hand=2},
            

            on_construct = function(pos)
                local pos, node, meta = upi.pptriple(pos)
                tier_nw:init_converter(pos, node, meta)
                meta:set_string("formspec", tier_nw:basic_converter_formspec(pos, node, meta))
            end,
            on_destruct = function(pos)
                local pos, node, meta = upi.pptriple(pos)
                tier_nw:destroy_converter(pos, node, meta)
            end,
            on_receive_fields = function(pos, _formname, fields, _sender)
                local pos, node, meta = upi.pptriple(pos)
                tier_nw:target_converter_receive_form(pos, node, meta, fields)
            end,
            technic_run = function (pos)
                local pos, node, meta = upi.pptriple(pos)
                tier_nw:refresh_network_information_converter(pos, node, meta)
                -- Reset/refresh the formspec
                meta:set_string("formspec", tier_nw:basic_converter_formspec(pos, node, meta))
            end,
            on_upi_core_state = function(pos, node, meta, has_core_now)
                if has_core_now and node.param2 == 0 then
                    node.param2 = 1
                    minetest.swap_node(pos, node)
                elseif not has_core_now and node.param2 == 1 then
                    node.param2 = 0
                    minetest.swap_node(pos, node)
                end
            end
        }, upi.api.contype.tgt)

        -- Register source recipes
        minetest.register_craft({
            output = src_node_name,
            recipe = {
                {"basic_materials:copper_wire", "upi:interconnect_core", cable_name}
            },
            replacements = {
                {"basic_materials:copper_wire", "basic_materials:empty_spool"}
            }
        })
    
        -- Same ingredients and we provide easy switcharound recipe ;p nya
        minetest.register_craft({
            output = src_node_name,
            type = "shapeless",
            recipe = {tgt_node_name}
        })

        -- Register target recipes ^.^
        minetest.register_craft({
            output = tgt_node_name,
            recipe = {
                {cable_name, "upi:interconnect_core", "basic_materials:copper_wire"}
            },
            replacements = {
                {"basic_materials:copper_wire", "basic_materials:empty_spool"}
            }
        })
        
        minetest.register_craft({
            output = tgt_node_name,
            type = "shapeless",
            recipe = {src_node_name}
        })

    end
end

if upi.api.enable_converters["technic"] then define_converters() end



-- upi
-- Copyright (C) 2021  sapient_cogbag <sapient_cogbag@protonmail.com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
