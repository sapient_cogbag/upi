# UPI
U(niversal) P(ower) I(nterface) enables interoperability between minetest mod power systems, and provides a general
framework that accounts for the different types of power systems mods use in a (hopefully) fairly flexible manner for relative ease of both adding converters and generally providing a standard method of using those power systems.

Developers are encouraged to make pull requests adding new networks to the mod (once the core API for specifying the types of network and functions for interfacing with the converter core are complete). When doing so, using UPI-factors (the amount of UJ for one unit of the mod's power system) with large numbers of factors is preferred
as it allows other mods to specify ratios in a clean fashion nya. Any non-integer power generation gets `floor`ed so 
it is also power-wasteful nya (though if you want to deliberately generate inefficiency, using low-factor numbers is a way to do it).

## Core Concepts
For this mod to be effective, a few separate parts are required to function together in a very particular manner. The concepts required to understand how this works are listed below nya.

### Conversion Direction

In order to meaningfully talk about a converter between two power networks, we need to specify some terminology: 
* The "source" network is the network from which power is being taken nya. On this network, a converter block 
needs to register demand for power or energy. The relevant block is known as a "source converter".
* The "target" network is the network to which power is supplied. On this network, a converter block needs to register that it is supplying power or energy. The relevant block is known as a "target converter". nya

UPI converters are "target driven, source limited", which roughly means that the target converter determines the 
amount of power that it "wants" to generate and then the source converter limits that based on the available power 
and user settings. A combined source/target converter core can also throttle power throughput if so desired.

UPI converters are a power based network (allbeit a very trivial one)


### Units
To implement this mod efficiently, it is necessary to use a universal power unit which other power systems can translate to and from.

The units in question are the following:
* UJ is a upiJoule. Multiples are expressed like KUJ, MUJ, GUJ. It represents supplying one UW for one second
* UW is a upiWatt.

Both are required due to the potential for multiple types of power system. In particular - some power systems primarily function using units of power and don't bother with energy except in the form of the occasional battery or similar.

Other power systems may use block energy transfer, and hence interfacing with both power system types is an important capability. It also allows the creation of mods that can meaningfully store UJ for transfer via non-powerline means. nya.

### Binary/Non-Binary Systems

When power systems run low on power, there are two main different responses on a per-block level:
* reduce functionality - for instance a furnace cooking slower or a light dimming nyaa - this is a non-binary system.
* immediately turn off when the power requirements can't be fulfilled, which itself can manifest in one of two main ways (this is a binary power system): 
    * All the blocks turn off - this is a global binary system.
    * Power redirects to some subsection of blocks nya - this is a local binary system.

Energy based systems can only ever (sensibly) do the latter option, because the consumption rate of machines is not 
tallied by the network, instead only the amount of energy stored or available in machines. Power based systems can do both.

### Supply and Demand Issues

Different power systems have different notions of supply and demand and whether supply or demand is fulfilled. This can start to become quite a mess in terms of specifying, so we need to first enumerate the ways this can be managed nyaaa.

##### Energy Based Systems
Energy based systems have a very simple manner of supply and demand - you just periodically spit out the supply of some charge onto the network or demand charge from it.

##### Power Based Systems
Power based systems are far more complex in many respects on both the supply and demand end. In the simplest case, a 
machine on the power network simply sets the amount of power it is currently providing or how much it demands. 

In more complex cases, machines can specify how much power they need to run to determine whether their demand is fulfilled, and when it is not this has the option of the block being made to not run (which doesn't make much sense for a power converter). Supply can also sometimes have specified limits or potential maximal output, but unlike demand whether or not that is fulfilled to maximum is not usually so important to keeping the network running nya.

## Capabilities/Modes of Source Converters

Source converters are the nodes which consume power from a network and turns it into upiWatts. Different types of power networks provide different options that are available to the user of the blocks nyaa.

##### Energy-Based Source Network
Energy source converters contain an energy buffer that has room for 10 seconds worth of energy supplying the maximum demand recorded nya. Every second, if there is enough energy for a second of power at the current demand, we take the amount of energy to supply that demand for 1 second from the pool. If not, we supply nothing. If the demand changes, then the energy pool gets an updated size of 10 * demand if that is bigger than the current capacity nya. In all modes of energy network, the options are as follows:

* Unlimited - request as much energy as needed to fill the buffer, every second, and take whatever is sent unless
   the buffer is filled up (in which case send the remainder back if possible nya).
* Rate Matching - every second request exactly the amount of energy required to fulfil the current demand.
* Off - disable obtaining energy at all.

##### Power-Based Source Network
With power-based source networks, the supplied power simply matches the appropriate amount from the network nya. If the supplied power is non-integer, then it is `floor`ed after being multiplied by the UPI-factor.

Selecting the appropriate amount from the network is more complex, however, and hence the source converter has the following options for controlling how much to demand from source or just take:
* Always Match Target Demand - In this case, always demand enough from source to supply the amount that the 
  target converter demands. In globally binary networks, this option may cause the source network to shut 
  down due to overloading nya.
* Limit to Unused Capacity - This is only available on networks with easily readable remaining power 
  capacity. This is a good option for globally binary networks, as it ensures that the amount demanded    
  from the source network can never exceed the available capacity nya. In a non-binary source network, 
  this would essentially prioritise the source converter lower than all other machines on the network 
  for power access.
* Limit to Available Capacity - Only available on networks where the total generation capacity is available. 
  This limits the demand on the source network to the *total* capacity of 
  the network even if some of it is being used by other machines. On a non-binary network, this 
  essentially gives the same priority as any other machine in it's place would have for power.
* Off - disable obtaining power whatsoever.

## Capabilities/Modes of Target Converters

Target converters are the nodes that demand upiWatts and convert them into appropriate forms of energy or power. All
of them, along with these other options, has an option to turn them off nya.

##### Energy-Based Target Network
The mode of output for energy based target networks is simple - just keep a buffer of some size in upiJoules, and periodically (once per second) dump 1/10th of it onto the network (and keeping any that there isn't room for if that is the case nya). The difficulty then is specifying:
* How big the buffer should be.
* How much power to demand from the source converter.

Some energy networks may periodically ping for producers, in which case it is acceptable to only produce energy then. 

In order to ensure that no free energy weirdness happens if the actual *provided* energy changes, certain particularly finnicky things must happen nya. Firstly, every time the buffer is ejected onto the network, a time (from `minetest.get_us_time()`) is saved as metadata. Before this, the previous time that something was *added* to the buffer was recorded, hence it is possible to derive the total energy input over time and add it to the buffer nya (being careful to check if new_time - old_time > 0 since it can overflow on some systems). If the provided 
power changes, the same buffer addition operation can occur to ensure no extra power comes through, again setting the
time.

When emitting the energy buffer (or part of it) onto the target network, the upiJoules are divided by the UPI ratio, then `floor`ed before being emitted nya. Then any energy received back (if the network does that to account for lack of room for the energy) is converted back with a multiply. Note - the buffer never shrinks in size in the case of 
dynamic buffers. 

For energy target converters, there are two sets of options.

*Buffer Size Options*
* Fixed - One of 1kUJ, 10kUJ, 100kUJ, 1MUJ, 10MUJ, 100MUJ, 1GUJ, 10GUJ, 100GUJ, 1TUJ
* Storage Capacity - If the target network has some kind of indication of storage capacity, this mode updates the
  internal buffer to match it in size (rounded up from the UPI multiplication).
* Remaining Storage Capacity - If the target network has an indication of unfilled storage capacity, update the 
  buffer to match it (rounded up from the UPI multiplication nyaa)

*Power Demand Options*
* Fixed - user-enterable upiWatt demand that keeps pulling power until the buffer is full nya 
* 10s Buffer Fill - demand the amount of power required for the buffer to fill in 10 seconds if it was unemptied nya (of course until it is full nya)
* 10s Balanced Fill - Same as 10s buffer fill except if the buffer drops below 20%, change the demand to an amount 
  able to fill the buffer within a second nya, and if it goes above 80% don't demand anything at all.

##### Power-Based Target Network
Outputting to power-based networks is also simple nya. All that is needed is to take the power provided by the source converter, 
perform the appropriate division, then `floor` that nya. The important part here is how the upi-demand to pass to the source 
network is calculated. For this, there are a few options: 
* Match Unmet Demand - This option is available if the target network provides a method to determine the demand that is not supplied by existing power sources. When the unmet demand is determined, it then is converted to upiWatts and `ceil`ed, to determine the demand nya.
* Match All Demand - This option is available if the target network provides a method to determine the total demand for power in the network.
* Fixed - This option is always available and allows entry of a fixed number of upiWatts to supply/upi-demand-from-source.


## LICENSING
The project itself is licensed under GPLv3 or later versions.
All media is licensed under CC-BY-SA when it was made by me nya.
* The hv/mv/lv wire bits on the source and target converters are derived from the technic mod textures (though i can't find a specific license).
