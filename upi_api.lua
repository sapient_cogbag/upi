local S = upi.translator

local default_target_mode_fieldname = "upi:target_mode_index"
local default_target_fixed_demand_fieldname = "upi:target_fixed_demand_number"
local default_source_mode_fieldname = "upi:source_mode_index"


upi.registry = {}

-- Registry of all power and energy networks nya
upi.registry.networks = {}


-- Map of all converters: node name -> {
--  .nw_name = network_name, 
--  .converter_type = converter type from upi.api.contype
-- }
upi.registry.converters = {}


upi.api.nw = {}

-- Power-Based or Energy-Based networks nya
upi.api.nw.nettype = {}
upi.api.nw.nettype.power = "power"
upi.api.nw.nettype.energy = "energy"

-- Get the network that was registered with the name network_name
--
-- For all networks, see `nw_meta` below for the available functions
--
-- For power-based networks (the only ones we currently support nyaa) see power_nw_meta
--
-- If the name is not valid, return `nil`
function upi.api.nw.get_registered_network(network_name)
    return upi.registry.networks[network_name]
end


-- Scheme taken roughly from technic mod nya
--
-- Represents the type of connection to the power network.
upi.api.contype = {}
upi.api.contype.receiver = "RE"
upi.api.contype.provider = "PR"

-- Aliases for converters

-- Specify that we want to receive from the registered power network
-- (this is a source converter nya)
upi.api.contype.src = upi.api.contype.receiver
-- Specify that we want to provide to the registered power network 
-- (this is a target converter)
upi.api.contype.tgt = upi.api.contype.provider


-- Options for network converters.
upi.api.opts = {}

-- Power-based opts
upi.api.opts.pow = {src = {}, tgt = {}}
-- Energy-based opts
upi.api.opts.eng = {src = {}, tgt = {}}

-- Source options
upi.api.opts.pow.src.match_target_demand = "Match Target Demand"
upi.api.opts.pow.src.limit_to_unused_capacity = "Limit to Unused Production Capacity"
upi.api.opts.pow.src.limit_to_available_capacity = "Limit to Available Production Capacity"
upi.api.opts.pow.src.off = "Off"

-- Target options
upi.api.opts.pow.tgt.match_unmet_demand = "Match Unmet Power Consumption"
upi.api.opts.pow.tgt.match_all_demand = "Match Total Power Consumption"
upi.api.opts.pow.tgt.fixed = "Fixed Demand"
upi.api.opts.pow.tgt.off = "Off"



-- Get the network associated with a given node name - that is the type of network it 
-- serves or takes power/energy from nya. If it has no network, of course return nil nya
function upi.api.get_network_class(node_name)
    local conv = upi.registry.converters[node_name]
    if conv then return upi.registry.networks[conv.nw_name] end
end

-- Get the converter type of nodes with the given name if it is a converter, else return nil
function upi.api.get_converter_type(node_name)
    local conv = upi.registry.converters[node_name]
    if conv then return conv.converter_type end
end


-- {{{ Supply and demand conversion between upi and native power nya
-- Get the native demand to place/being placed on a power network for a given upi demand
function upi.api.get_native_power_demand(upi_demand, ratio)
    return math.ceil(upi_demand/ratio)
end

-- Get the upi power provided from the given amount of native power provided by the network
function upi.api.get_upi_power_supplied(native_supplied, ratio)
    return math.floor(native_supplied * ratio)
end

-- Get the amount of upi power demanded if given an amount that is demanded natively
function upi.api.get_upi_power_demanded(native_demanded, ratio)
    return math.ceil(native_demanded * ratio)
end

-- Get the amount of native power supplied when supplied with the given upi power nya
function upi.api.get_native_power_supplied(upi_supplied, ratio)
    return math.floor(upi_supplied / ratio)
end

-- }}}



-- Local metatable for all networks nyaa
local nw_meta = {}

-- Register the given node definition being able to connect to this type of power network
-- with the given connection type (see upi.api.contype)
--
-- This actually does the full minetest.register_node monty, while applying modifications 
-- to the node definition if necessary.
function nw_meta:register(node_name, definition, connection_type)
    assert(
        connection_type == upi.api.contype.src or connection_type == upi.api.contype.tgt, 
        "Invalid converter type. NYA!"
    )

    local def = definition
    if type(self.modify_register_node_definition) == "function" then
        def = self.modify_register_node_definition(node_name, def, connection_type)
    end

    minetest.register_node(node_name, definition)

    if type(self.register_connectable) == "function" then
        self.register_connectable(node_name, connection_type)
    end
end


-- Register a converter for this network nya, with the given node definition
--
-- This is used for registering source and target converters. The internals for 
-- source and target converter methods will:
--  * If something happens to indicate that there *is* a core, then call 
--    definition.on_upi_core_state(pos, node, meta, has_core) with has_core = true
--  * If something happens to indicate that there *isn't* a core, then 
--    call definition.on_upi_core_state(pos, node, meta, has_core) with has_core = false
-- the (pos, node, meta) triple refers to the current converter, not the core
function nw_meta:register_converter(node_name, definition, connection_type)
    self:register(node_name, definition, connection_type)
    minetest.log("action", "Registering Converter [type - " .. connection_type .. "; name - " .. node_name .. "]")
    upi.registry.converters[node_name] = {
        nw_name = self.name,
        converter_type = connection_type 
    }
end


-- Perform any operations required on general initialisation of nodes that connect to this power 
-- network
function nw_meta:init(pos, node, meta)
    if type(self.init_node) == "function" then
        if node then self.init_node(pos, node, meta) end
    end
end
-- Perform any operations required on node construction for a converter^.^. This MUST be called.
--
-- Optionally provide node and meta args if you already have them :)
--
-- Note that if the position is out of bounds/nil/ungenerated we do nothing
function nw_meta:init_converter(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    self:init(pos, node, meta) 
    local conn_type = upi.registry.converters[node.name].converter_type

    if conn_type == upi.api.contype.src then
        upi.ipnw.source.initialise(pos, node, meta)
    elseif conn_type == upi.api.contype.tgt then
        upi.ipnw.target.initialise(pos, node, meta)
    end
end

-- Perform any operations required on node destruction nya
--
-- Optionally provide node or meta as well
--
-- This MUST be called.
function nw_meta:destroy(pos, node, meta) 
    if type(self.on_destroy_node) == "function" then
        local pos, node, meta = upi.pptriple(pos, node, meta)
        if node ~= nil then
            self.on_destroy_node(pos, node, meta)
        end
    end
end

-- Perform the operations required to destroy a converter ^.^
function nw_meta:destroy_converter(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    self:destroy(pos, node, meta)
    local conn_type = upi.registry.converters[node.name].converter_type
    if conn_type == upi.api.contype.src then
        upi.ipnw.source.on_destroy(pos, node, meta)
    elseif conn_type == upi.api.contype.tgt then
        upi.ipnw.target.on_destroy(pos, node, meta)
    end
end

-- Whether or not we can test for network connection.
function nw_meta:can_test_connection()
    return type(self.is_network_connected) == "function"
end

-- Test if the network is connected, and if it is, return a representation of it if 
-- available or `true` if not available, and `false` or `nil` if not connected nya.
--
-- If `node` is `nil` we attempt to get it from position, and if `meta` is nil we also do
-- the same nya
--
-- If we can't test for network connection, then we return "indeterminable"
function nw_meta:network_connected(pos, node, meta)
    if self:can_test_connection() then
        local pos, node, meta = upi.pptriple(pos, node, meta)
        return self.is_network_connected(pos, node, meta)
    else
        return "indeterminable"
    end
end

-- {{{ Source and Target Converter Modes nya

-- Test if the given mode is valid for a source converter for the given network
--
-- If it is, then return the contents of the mode table for the mode nya
function nw_meta:is_source_mode(mode)
    return self.allowed_source_modes[mode]
end

-- Get the default source mode name
function nw_meta:get_default_source_mode() 
    return self.default_source_mode
end

-- Test if the given mode is valid for a target converter for the given network
--
-- If it is, then return the contents of the mode table for the mode
function nw_meta:is_target_mode(mode)
    return self.allowed_target_modes[mode]
end

-- Get the default target mode name nya
function nw_meta:get_default_target_mode()
    return self.default_target_mode
end

-- Get the names of the target converter modes in fixed order list form nya
--
-- Note that this order is not deterministic on reboots/restarts (in case the lua engine
-- causes shift in table orders). To store converter modes, simply store the actual
-- mode, don't use the index given here nya.
function nw_meta:get_target_converter_mode_names()
    return self.target_mode_names
end

-- Get the names of the source converter modes in fixed order list form
--
-- Note that this order is not deterministic on reboots/restarts (in case the lua engine
-- causes shift in table orders). To store converter modes, simply store the actual
-- mode, don't use the index given here nya.
function nw_meta:get_source_converter_mode_names()
    return self.source_mode_names
end

-- Get the index of the given target converter mode in the table returned by 
-- `self:get_target_converter_mode_names()`
--
-- If it is not a valid mode, return `nil`
function nw_meta:get_target_converter_mode_index(mode_name)
    return self.target_mode_indices[mode_name]
end


-- Get the index of the given source converter mode in the table returned by 
-- `self:get_source_converter_mode_names()`
--
-- If it is not a valid mode, return `nil`
function nw_meta:get_source_converter_mode_index(mode_name)
    return self.source_mode_indices[mode_name]
end


-- Get a translated list of target converter mode names in the order
-- of the indices of self:get_target_converter_mode_names(), which are
-- formspec delimited.
function nw_meta:get_target_converter_mode_translated_list()
    local result = {}
    for idx, name in ipairs(self:get_target_converter_mode_names()) do
        result[idx] = minetest.formspec_escape(S(name))
    end
    return result
end


-- Get a translated list of source converter mode names in the order
-- of the indices of self:get_source_converter_mode_names(), which are
-- formspec delimited.
function nw_meta:get_source_converter_mode_translated_list()
    local result = {}
    for idx, name in ipairs(self:get_source_converter_mode_names()) do
        result[idx] = minetest.formspec_escape(S(name))
    end
    return result
end

-- }}}

-- Get whether this is a power or energy-based network nya
function nw_meta:get_network_base()
    return self.network_type
end


-- Refresh information from the power network into the given source or target converter
--
-- This needs to be called:
--  * When the source/target network connects or disconnects
--  * When the mode changes
--  * Power network changes, there are events, or just general power network ping if that's
--      easiest nya.
--  * TODO: specify criteria for energy networks
function nw_meta:refresh_network_information_converter(pos, node, meta) 
    local pos, node, meta = upi.pptriple(pos, node, meta)
    
    local conn_type = upi.registry.converters[node.name].converter_type

    if conn_type == upi.api.contype.src then
        upi.ipnw.source.refresh_all_information(pos, node, meta)
    elseif conn_type == upi.api.contype.tgt then
        upi.ipnw.target.refresh_all_information(pos, node, meta)
    end
end


-- Metatable setup (override)
--
-- Only called if key isn't present so...
function nw_meta.__index(tb, key)
    return nw_meta[key]
end


-- Metatable for power networks (has basic network functions
-- also)
local power_nw_meta = setmetatable({}, nw_meta)

-- Power network metatable :) - override access nya
function power_nw_meta.__index(tb, key)
    return power_nw_meta[key]
end

-- Metatable for energy networks (has basic network functions 
-- also)
local energy_nw_meta = setmetatable({}, nw_meta)

function energy_nw_meta.__index(tb, key)
    return energy_nw_meta[key]
end



-- Power network interfacing nya

-- {{{ Functions for power receivers

-- This function sets the amount of power to demand from the power network - in NATIVE units
--
-- It checks network connectivity as well if it can. If not connected then the demand does
-- NOT get changed - so make sure to call this on network connection nya
function power_nw_meta:demand_native(pos, node, meta, new_demand)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    local net = self:network_connected(pos, node, meta)
    if not net then return end
    self.set_source_demand(pos, node, meta, net, new_demand)
end

-- The same as the demand function but it uses upiWatts as its unit (and of course does
-- appropriate rounding up nya
--
-- Remember that this only applies the demand if connected to a network, so on connection
-- ensure that you apply the demand anew nya
function power_nw_meta:demand_upi(pos, node, meta, new_upi_demand)
    local native_demand = upi.api.get_native_power_demand(new_upi_demand, self.ratio)
    self:demand_native(pos, node, meta, native_demand)
end

-- Get the amount of power supplied to this node by the network in native power units. 
-- If not connected then it returns 0
function power_nw_meta:supplied_power_native(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    local net = self:network_connected(pos, node, meta)
    if not net then return 0 end
    return self.get_received_power(pos, node, meta, net)
end

-- Get the amount of power supplied to this in upiWatts
function power_nw_meta:supplied_power_upi(pos, node, meta)
    local provided = self:supplied_power_native(pos, node, meta)
    return upi.api.get_native_power_supplied(provided, self.ratio)
end

--}}}


-- {{{ Functions for power suppliers

-- Supply the given amount of power to the network - if the network is not connected then 
-- do nothing nya - so make sure to set this when connected or disconnected or whatever
function power_nw_meta:supply_native(pos, node, meta, new_supply)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    local net = self:network_connected(pos, node, meta)
    if not net then return end
    self.set_target_supply(pos, node, meta, net, new_supply)
end

-- Supply the given amount of power (specified in upiWatts) to the network. If not connected
-- this will do nothing
function power_nw_meta:supply_upi(pos, node, meta, new_upi_supply)
    local native_supply = upi.api.get_native_power_supplied(new_upi_supply, self.ratio)
    self:supply_native(pos, node, meta, native_supply)
end

-- }}}


-- {{{ Power network source/target formspec & handlers

-- Build a dropdown for target converter modes. It requires formspecv4 because it uses 
-- the dropdown index mode (this provides translation capability since we don't have to
-- check strings nya).
--
-- Arguments:
--  * Standard pos/node/meta triple specifying the target converter
--  * X, Y, W are the position and width... nya
--  * field_name - The name of the field that the index is injected into
--  * preprocess_entry, if not nil,  takes (pos, node, meta) and entry string (formspec delimited) and should 
--    return a new string that is actually used in the resulting formspec code nyaaa
--    This can be used e.g. for formatting.
--
--  
function power_nw_meta:build_target_converter_mode_dropdown(
    pos, node, meta, 
    field_name, 
    X, Y, W,
    preprocess_entry
)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    local curr_mode = upi.ipnw.bidi.get_mode(pos, node, meta)
    local idx = self:get_target_converter_mode_index(curr_mode)

    local result = string.format(
        "dropdown[%s, %s; %s; %s;",
        tonumber(X),
        tonumber(Y),
        tonumber(W),
        field_name
    ) 

    local preprocessed = {}
    for i, entry in ipairs(self:get_target_converter_mode_translated_list()) do
        if type(preprocess_entry) == "function" then
            preprocessed[i] = preprocess_entry(pos, node, meta, entry)
        else
            preprocessed[i] = entry
        end
    end

    result = result .. table.concat(preprocessed, ",") .. ";" .. tostring(idx) .. ";true]"
    return result 
end


-- Build a dropdown for source converter modes. It requires formspecv4 because it uses 
-- the dropdown index mode (this provides translation capability since we don't have to
-- check strings nya).
--
-- Arguments:
--  * Standard pos/node/meta triple specifying the source converter
--  * X, Y, W are the position and width... nya
--  * field_name - The name of the field that the index is injected into
--  * preprocess_entry, if not nil,  takes (pos, node, meta) and entry string (formspec delimited) and should 
--    return a new string that is actually used in the resulting formspec code nyaaa
--    This can be used e.g. for formatting.
--
--  
function power_nw_meta:build_source_converter_mode_dropdown(
    pos, node, meta, 
    field_name, 
    X, Y, W,
    preprocess_entry
)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    local curr_mode = upi.ipnw.bidi.get_mode(pos, node, meta)
    local idx = self:get_source_converter_mode_index(curr_mode)

    local result = string.format(
        "dropdown[%s, %s; %s; %s;",
        tonumber(X),
        tonumber(Y),
        tonumber(W),
        field_name
    ) 

    local preprocessed = {}
    for i, entry in ipairs(self:get_source_converter_mode_translated_list()) do
        if type(preprocess_entry) == "function" then
            preprocessed[i] = preprocess_entry(pos, node, meta, entry)
        else
            preprocessed[i] = entry
        end
    end

    result = result .. table.concat(preprocessed, ",") .. ";" .. tostring(idx) .. ";true]"
    return result 
end

-- Generate a simple formspec (v4) string for target converters nya.
--
-- The (width, height) is (8, 3) units in real coordinates, and it is positioned at (0, 0).
-- To move it, use container[] or whatever ;p. If the fieldnames are not provided,
-- the defaults are 'upi:target_mode_index' and 'upi:target_mode_fixed_demand_number'
--
-- This must be put inside a full formspec.
function power_nw_meta:target_formspec_content(
    pos, node, meta, 
    target_mode_fieldname,
    target_fixed_demand_number_fieldname
) 
    local target_mode_fieldname = target_mode_fieldname or default_target_mode_fieldname
    local target_fixed_demand_number_fieldname = target_fixed_demand_number_fieldname or default_target_fixed_demand_fieldname
    local pos, node, meta = upi.pptriple(pos, node, meta)
    local dropdown = self:build_target_converter_mode_dropdown(
        pos, node, meta, 
        target_mode_fieldname, 
        0.5, 0.25, 
        7
    )
    local field_entry = 
        "field[0.5,2;4,0.75;" .. target_fixed_demand_number_fieldname .. ";" .. 
        S("Fixed Demand (UW):") .. ";${" .. upi.ipnw.meta.target.fixed_demand_number .. "}]"
    return dropdown .. field_entry
end

-- Generate a simple formspec (v4) string for source converters nya.
--
-- The width, height is (8, 2) units in real coordinates, and it is positioned at (0, 0).
-- To move it, use container[] or whatever ;p. If the fieldname is not provided
-- the default is 'upi:source_mode_index'.
--
-- This must be put inside a full formspec.
function power_nw_meta:source_formspec_content(
    pos, node, meta, 
    source_mode_fieldname
) 
    local source_mode_fieldname = source_mode_fieldname or default_source_mode_fieldname
    local pos, node, meta = upi.pptriple(pos, node, meta)
    local dropdown = self:build_source_converter_mode_dropdown(
        pos, node, meta, 
        source_mode_fieldname, 
        0.5, 0.25, 
        7
    )
    return dropdown
end

-- Generate a basic default formspec for the given source or target
--
-- This uses formspec v4 nya, with the default field names. This should probably
-- be called whenever there is a power network change or mode change
--
-- If this isn't a converter, return nil
function power_nw_meta:basic_converter_formspec(pos, node, meta)
    local pos, node, meta = upi.pptriple(pos, node, meta)
    local convtype = upi.api.get_converter_type(node.name)
    if convtype then
        if convtype == upi.api.contype.src then
            return "formspec_version[4]size[8,4]" .. self:source_formspec_content(pos, node, meta)
        elseif convtype == upi.api.contype.tgt then
            return "formspec_version[4]size[8,4]" .. self:target_formspec_content(pos, node, meta)
        end
    end
end

-- Parse a source form input with incoming fields.
--
-- This takes a (pos, node, meta) and the (fields) table.
--
-- It also optionally takes the name of the mode index field nya. If that is nil, it uses the 
-- default
--
-- This should always be called on receiving a form for a source converter, it is necessary 
-- to allow the user to update the mode nya
function power_nw_meta:source_converter_receive_form(pos, node, meta, fields, source_mode_fieldname)
    local pos, node, meta = upi.pptriple(pos, node, meta)

    local index = fields[source_mode_fieldname or default_source_mode_fieldname]
    if not index then return end

    local new_mode = self:get_source_converter_mode_names()[index]
    if not new_mode then return end

    upi.ipnw.source.set_mode(pos, node, meta, new_mode) 
end

-- Parse a target form input with incoming fields.
--
-- This takes a (pos, node, meta) and the (fields) table.
--
-- It also optionally takes the name of the mode index field nya, and fixed demand number field. 
-- If either is nil, it uses the default.
--
-- This should always be called on receiving a form for a target converter, it is necessary 
-- to allow the user to update the mode nya
function power_nw_meta:target_converter_receive_form(
        pos, node, meta, 
        fields, target_mode_fieldname, target_fixed_demand_number_fieldname
)
    local pos, node, meta = upi.pptriple(pos, node, meta)

    -- If BOTH of these are not present we want to dump out nya
    local index = fields[target_mode_fieldname or default_target_mode_fieldname]
    local fixed_demand = fields[target_fixed_demand_number_fieldname or default_target_fixed_demand_fieldname]
    if not index and not fixed_demand then return end

    -- the new mode to pass to the set_mode function.
    --
    -- If there wasn't one in the fields table, this will be nil anyhow and that's fine
    -- with set_mode
    local new_mode

    if index then
        new_mode = self:get_target_converter_mode_names()[index]
    end

    upi.ipnw.target.set_mode(pos, node, meta, new_mode, fixed_demand) 
end
-- }}}


-- Register a power-based network type.
--
-- The name should be in the form `modname:networkname`, for instance `technic:lv`.
-- The ratio should be a positive integer >0 (and this is checked and `floor`ed) which 
--  represents the number of UJ or UW per mod-power-system-energy-unit/power-unit.
-- The specification is a table with various (optional) entries:
--
--  * modify_register_node_definition(node_name, node_definition, connection_type) - This allows 
--    the network specification to modify the node definition before it gets fully
--    registered nya. This should return the new node definition table.
--
--  * register_connectable(node_name, connection_type) - if this is present, it should 
--    register the given node name to the power mod such that the machine will be able to 
--    connect with the given connection type (receiver, provider, bidirectional)
--
--    If set to `nil` then UPI does nothing.
--
--  * init_node(pos, node, meta) - Perform any initialisation per-node on construction required 
--    to connect to this type of power network or any other state for managing the power.
--
--    For instance, microexpansion does not by default provide a simple way to keep track 
--    of the per-machine demand and in fact works off accumulation. Hence for functions to 
--    change power demand, it may be required to keep track of the current demand, then
--    when changing demand, subtract that from the network and add the new demand nya.
--
--  * on_destroy_node(pos, node, meta) - Perform any operation needed on per-node 
--    destruction
--
--  * is_network_connected(pos, node, meta) - Get if the node is connected to this network nya.
--    This should return something "truth-y" - in particular, if the node isn't connected to 
--    the network it should return `false` or `nil`. If it is, however, then it is recommended 
--    to return something representing the specific network in question if possible (though
--    it is ok to return `true` if that's the only option nya). It is also possible to return
--    "indeterminable" if there are some cases where it is so
--
--    If this *isn't* provided, then it is impossible to test for network connection at 
--    all and any functions which take the network representation will get 'indeterminable'
--    for that parameter nya.
--  
-- In all the following functions, they are *only* called if the network is connected and 
-- the node is not `nil` (when using minetest.get_node_or_nil), except of course if there 
-- is no detection method for connectivity - in which case it will be the string 'indeterminable'
-- In those cases, you MUST return some value (probably zero), since this library does not
-- handle `nil` output nyaa.
--
-- Functions for manipulating the network as a `source` network. Not all of these are
-- optional. In all cases units of power are native to the network, not in upiWatts.
-- (conversion is *always* handled by UPI):
--  * set_source_demand [required] (pos, node, meta, net, new_demand) - set the demand of 
--      the given node on the source network nya
--
--  * get_unused_capacity (pos, node, meta, net) - return the unused power production capacity. 
--      Negative results are interpreted as 0 (i.e. the result is processed by 
--      max(0, value)). 
--
--      This must NOT include the power used by a source converter or other machine. If 
--      necessary, store the current demand in `set_source_demand` nya.
--
--  * get_total_capacity(pos, node, meta, net) - return the total available capacity of 
--      the network nyaa.
--
--  * get_received_power [required] (pos, node, meta, net) - return the actual amount of
--      power being provided by the network. 
--
-- Functions for manipulating the network as a `target` network. Not all of these are
-- optional. In all cases units of power are native to the network, not in upiWatts (again
-- conversion is handled by UPI nya):
--  * set_target_supply [required] (pos, node, meta, net, new_supply) - set the amount
--      of power actually being supplied to the target network.
--
--  * get_unmet_demand (pos, node, meta, net) - get the amount of power that is needed but 
--      not supplied. Negative numbers are treated as 0. 
--
--      This must NOT include the amount being supplied by the target converter or machine.
--      If necessary, store the amount being supplied when set_target_supply is called
--      (see: init_node)
--
--  * get_total_demand (pos, node, mets, net) - get the amount of power that is needed 
--      for the whole network nya
--
function upi.api.nw.register_power_network(name, ratio, specification)
    assert(
        type(specification.set_target_supply) == "function",
        "Missing required function for setting supply."
    )
    assert(
        type(specification.set_source_demand) == "function",
        "Missing required function for setting demand. Nya."
    )
    assert(type(name) == type("str ;p"), "Missing valid network name")
    assert(type(ratio) == type(0) and ratio >= 1, "Ratio must be positive (and is converted to int)")
    ratio = math.floor(ratio)

    specification.name = name
    specification.ratio = ratio
    specification.network_type = upi.api.nw.nettype.power

    local src = upi.api.opts.pow.src
    
    specification = setmetatable(specification, power_nw_meta)
    
    -- Calculate the allowed source and target modes nya
    
    -- Each source mode is a function that takes a (pos, node, meta, network, new_upi_demand) 
    -- argset and returns the current amount being provided (for an immediate update)
    --
    -- Note that de-nil-ification is applied to the pos, node, meta triple before calling. 
    -- No need to do that. nya
    local allowed_source_modes = {}
    allowed_source_modes[src.match_target_demand] = function (pos, node, meta, net, new_upi_demand) 
        -- No free energy, demand should be higher
        local sourcenet_demand = upi.api.get_native_power_demand(new_upi_demand, ratio)
        specification.set_source_demand(pos, node, meta, net, sourcenet_demand)
        return specification:supplied_power_upi(pos, node, meta)
    end

    allowed_source_modes[src.off] = function (pos, node, meta, net, new_upi_demand)
        -- EVERYTHING IS 0 NYA
        specification.set_source_demand(pos, node, meta, net, 0)
        return 0
    end

    if type(specification.get_unused_capacity) == "function" then
        allowed_source_modes[src.limit_to_unused_capacity] = 
            function (pos, node, meta, net, new_upi_demand)
                local unrestrained_sourcenet_demand = upi.api.get_native_power_demand(new_upi_demand, ratio)
                local unused_capacity = specification.get_unused_capacity(pos, node, meta, net)
                unused_capacity = math.max(unused_capacity, 0)
                local actual_demand = math.min(unused_capacity, unrestrained_sourcenet_demand)
                specification.set_source_demand(pos, node, meta, net, actual_demand)
                return specification:supplied_power_upi(pos, node, meta)
            end
    end

    if type(specification.get_total_capacity) == "function" then
        allowed_source_modes[src.limit_to_available_capacity] = 
            function (pos, node, meta, net, new_upi_demand)
                local unrestrained_sourcenet_demand = upi.api.get_native_power_demand(new_upi_demand, ratio)
                local capacity = specification.get_total_capacity(pos, node, meta, net)
                capacity = max(capacity, 0)
                local actual_demand = math.min(unrestrained_sourcenet_demand, capacity)
                specification.set_source_demand(pos, node, meta, net, actual_demand)
                return specification:supplied_power_upi(pos, node, meta)
            end
    end
    specification.allowed_source_modes = allowed_source_modes

    -- Each target mode is a function that takes the standard (pos, node, meta, net) 
    -- quadruple (no need to de-nil the components nya), as well as an argument "fixed" 
    -- which is the fixed value that can be provided by the user (in UW). It may be nil
    --
    -- These functions should return the new demand from the target in UW, or nil 
    -- if it can't change to that mode nya
    local allowed_target_modes = {}
    local tgt = upi.api.opts.pow.tgt
    
    allowed_target_modes[tgt.fixed] = function (pos, node, meta, net, vfixed)
        if vfixed == nil then return end
        return vfixed
    end

    allowed_target_modes[tgt.off] = function (pos, node, meta, net, _)
        return 0
    end

    if type(specification.get_unmet_demand) == "function" then
        allowed_target_modes[tgt.match_unmet_demand] = function (pos, node, meta, net, _)
            local unmet_demand = upi.api.get_upi_power_demanded(
                specification.get_unmet_demand(pos, node, meta, net), 
                ratio
            ) 
            return unmet_demand
        end
    end

    if type(specification.get_total_demand) == "function" then
        allowed_target_modes[tgt.match_all_demand] = function (pos, node, meta, net, _)
            local demand = upi.api.get_upi_power_demanded(
                specification.get_total_demand(pos, node, meta, net),
                ratio
            )
            return demand
        end
    end
    specification.allowed_target_modes = allowed_target_modes

    -- Defaults Nya
    
    if allowed_source_modes[src.limit_to_unused_capacity] ~= nil then
        specification.default_source_mode = src.limit_to_unused_capacity
    else
        specification.default_source_mode = src.match_target_demand
    end


    if allowed_target_modes[tgt.match_unmet_demand] ~= nil then 
        specification.default_target_mode = tgt.match_unmet_demand
    elseif allowed_target_modes[tgt.match_all_demand] ~= nil then
        specification.default_target_mode = tgt.match_all_demand
    else
        specification.default_target_mode = tgt.off
    end

    -- We provide some indexed modes - in particular so that there is a consistent 
    -- order for use in formspecs nya
    specification.target_mode_names = {}
    specification.source_mode_names = {}

    for modename, _ in pairs(specification.allowed_target_modes) do
        table.insert(specification.target_mode_names, modename)
    end

    for modename, _ in pairs(specification.allowed_source_modes) do
        table.insert(specification.source_mode_names, modename)
    end

    -- Inverse indexing table nya

    specification.target_mode_indices = {}
    specification.source_mode_indices = {}

    for i, name in ipairs(specification.target_mode_names) do
        specification.target_mode_indices[name] = i
    end

    for i, name in ipairs(specification.source_mode_names) do
        specification.source_mode_indices[name] = i
    end

    upi.registry.networks[name] = specification
end



-- upi
-- Copyright (C) 2021  sapient_cogbag <sapient_cogbag@protonmail.com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
